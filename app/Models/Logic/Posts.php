<?php

namespace App\Models\Logic;

class Posts
{
   public static function makeExcerpt($body, $length = 5){
    return substr($body, 0, $length);
   }
}