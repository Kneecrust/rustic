<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Post::factory(5)->create();

        // User::truncate();
        // Category::truncate();
        // Post::truncate();



        // $user = \App\Models\User::fac tory()->create();

        // $personal = Category::create([
        //     'name' => 'Personal',
        //     'slug' => 'personal'
        // ]);

        // $family = Category::create([
        //     'name' => 'Family',
        //     'slug' => 'family'
        // ]);

        // $work = Category::create([
        //     'name' => 'Work',
        //     'slug' => 'work'
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $family->id,
        //     'title' => 'Mi Famiglia',
        //     'slug' => 'mi-famiglia',
        //     'excerpt' => 'Pasta pasta Bolognese!',
        //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, enim sit amet vulputate placerat, diam nisl accumsan justo, malesuada maximus ligula tellus quis sem. Cras sed nisl aliquam, aliquam nisi ut, lacinia nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla facilisi. Mauris rhoncus dui feugiat nulla euismod, nec luctus lorem consequat. Maecenas eu erat elit. Nullam feugiat ultrices urna, id imperdiet felis pulvinar ut.</p>',
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $work->id,
        //     'title' => 'Le Work',
        //     'slug' => 'le-work',
        //     'excerpt' => 'Je work donc, je suis.',
        //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, enim sit amet vulputate placerat, diam nisl accumsan justo, malesuada maximus ligula tellus quis sem. Cras sed nisl aliquam, aliquam nisi ut, lacinia nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla facilisi. Mauris rhoncus dui feugiat nulla euismod, nec luctus lorem consequat. Maecenas eu erat elit. Nullam feugiat ultrices urna, id imperdiet felis pulvinar ut.</p>',
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $personal->id,
        //     'title' => 'Poezen',
        //     'slug' => 'poes-poes',
        //     'excerpt' => 'Yes, I like to pet kitties.',
        //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, enim sit amet vulputate placerat, diam nisl accumsan justo, malesuada maximus ligula tellus quis sem. Cras sed nisl aliquam, aliquam nisi ut, lacinia nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla facilisi. Mauris rhoncus dui feugiat nulla euismod, nec luctus lorem consequat. Maecenas eu erat elit. Nullam feugiat ultrices urna, id imperdiet felis pulvinar ut.</p>',
        // ]);
    }
}
