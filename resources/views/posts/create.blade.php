<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-6xl mx-auto mt-10 lg:mt-20 space-y-6">
            <article class="max-w-4xl mx-auto lg:grid lg:grid-cols-12 gap-x-10">
                <div class="col-span-12 lg:text-center lg:pt-14 mb-10">
                    <div class="hidden lg:flex justify-between mb-6">
                        <a href="/"
                            class="mt-4 transition-colors duration-300 relative inline-flex items-center text-lg hover:text-blue-500">
                            <svg width="22" height="22" viewBox="0 0 22 22" class="mr-2">
                                <g fill="none" fill-rule="evenodd">
                                    <path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M21 1v20.16H.84V1z">
                                    </path>
                                    <path class="fill-current"
                                        d="M13.854 7.224l-3.847 3.856 3.847 3.856-1.184 1.184-5.04-5.04 5.04-5.04z">
                                    </path>
                                </g>
                            </svg> Back to Posts
                        </a>
                    </div>
                    <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                        <h1 class="mb-2 font-bold text-2xl lg:text-3xl">Create a New Post</h1>
                        <p class="mb-5 block text-gray-400 text-xs">Fill and submit this form to create a post</p>

                        <hr>
                        <div>
                            <form action="{{route('posts.store')}}" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="1">
                                <div class="row">
                                    <div class="control-group col-12">
                                        <label for="title">
                                            <h5 class="font-bold">Post Title</h5>
                                        </label>
                                        <input type="text" id="title" class="form-control" name="title"
                                            placeholder="Enter Post Title" required>
                                    </div>

                                    <div class="control-group col-12">
                                        <label for="slug">
                                            <h5 class="font-bold">Post Slug</h5>
                                        </label>
                                        <input type="text" id="slug" class="form-control" name="slug"
                                            placeholder="Enter Post Slug" required>
                                    </div>

                                    <div class="control-group col-12">
                                        <label for="category">
                                            <h5 class="font-bold">Category</h5>
                                        </label>
                                        <select id="category" class="form-control" name="category_id"
                                            placeholder="Pick A Post Category" required>
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}"> 
                                            {{$category->name}}
                                            </option>
                                            @endforeach

                                        </select>
                                    </div>

                                    <div class="control-group col-12 mt-2">
                                        <label for="body">
                                            <h5 class="font-bold">Post Body</h5>
                                        </label>
                                        <textarea id="body" class="form-control" name="body"
                                            placeholder="Enter Post Body" rows="" required></textarea>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="control-group col-12 text-center">
                                            <button type="submit"
                                                class="bg-green-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5">Create
                                                Post</button>
                                      
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </article>
        </main>
    </section>
</x-layout>